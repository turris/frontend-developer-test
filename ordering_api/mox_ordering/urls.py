from django.urls import include, path
from rest_framework import routers
from mox_ordering.mox_ordering import views

router = routers.DefaultRouter()
router.register(r"modules", views.ModuleViewSet)
router.register(r"orders", views.OrderViewSet)

# Wire up our API using automatic URL routing.
# Additionally, we include login URLs for the browsable API.
urlpatterns = [
    path("", include(router.urls)),
    path("api-auth/", include("rest_framework.urls", namespace="rest_framework")),
]
