from django.apps import AppConfig


class MOXOrderingConfig(AppConfig):
    name = "mox_ordering"
