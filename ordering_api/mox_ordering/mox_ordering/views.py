from rest_framework import viewsets

from mox_ordering.mox_ordering.models import Module, Order
from mox_ordering.mox_ordering.serializers import ModuleSerializer, OrderSerializer


class ModuleViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = Module.objects.all().order_by("id")
    serializer_class = ModuleSerializer


class OrderViewSet(viewsets.ModelViewSet):
    queryset = Order.objects.all().order_by("id")
    serializer_class = OrderSerializer
