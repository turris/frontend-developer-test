from django.db import models


class Module(models.Model):
    name = models.CharField(max_length=50, unique=True)
    features = models.CharField(max_length=100, blank=True)

    def __str__(self):
        return f"{self.name} - {self.features}"


class Submodule(models.Model):
    name = models.CharField(max_length=50, unique=True)
    features = models.CharField(max_length=100, blank=True)
    parent_modules = models.ManyToManyField(Module, related_name="submodules")

    def __str__(self):
        return f"{self.name} - {self.features}"


class Order(models.Model):
    modules = models.ManyToManyField(Module)
    submodules = models.ManyToManyField(Submodule, blank=True)
