from rest_framework import serializers

from mox_ordering.mox_ordering.models import Module, Submodule, Order


class SubmoduleSerializer(serializers.ModelSerializer):
    class Meta:
        model = Submodule
        fields = ("id", "name", "features")


class ModuleSerializer(serializers.ModelSerializer):
    submodules = SubmoduleSerializer(read_only=True, many=True)

    class Meta:
        model = Module
        fields = (
            "id",
            "name",
            "features",
            "submodules",
        )


class OrderSerializer(serializers.ModelSerializer):
    class Meta:
        model = Order
        fields = "__all__"
